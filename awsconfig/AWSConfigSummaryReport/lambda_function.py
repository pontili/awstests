import boto3
import logging

#setup simple logging for INFO
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#regions
AWS_Regions=["eu-west-1"]
CloudWatch_Region="eu-west-1"

#define the connection
cw = boto3.client('cloudwatch')

def lambda_handler(event, context):
	#check all regions
	for region in AWS_Regions:
		configconn = boto3.client('config',region_name=region)
		configsum = configconn.get_compliance_summary_by_config_rule()
		#check all vpns for each region
		logger.info(configsum)
		logger.info(configsum["ComplianceSummary"]["NonCompliantResourceCount"]["CappedCount"])
		cw.put_metric_data(
				Namespace='CustomConfig',
				MetricData=[
				{
				'MetricName': 'SummaryNonCompliant',
				'Dimensions': [
				{
				'Name': 'Services',
				'Value': 'All'
				}
				],
				'Value': configsum["ComplianceSummary"]["NonCompliantResourceCount"]["CappedCount"],
				'Unit': 'Count'
				},
				]
				)